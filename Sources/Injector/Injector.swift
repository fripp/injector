import Swinject

public enum InjectorError: Error {
	case unknownType
}

public final class Injector {
    public static let shared = Injector()

	private var assembler: Assembler?

	private init() {}

	public func register(_ assemblies: [Assembly]) {
		assembler = Assembler(assemblies)
	}

	public func resolve<T>(_ type: T.Type, name: String? = nil) throws -> T {
		guard let res = assembler?.resolver.resolve(type, name: name) else {
			throw InjectorError.unknownType
		}

		return res
	}
}
