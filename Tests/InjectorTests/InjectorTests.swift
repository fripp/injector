import XCTest
import Swinject
@testable import Injector

protocol ProtocolWithNoImplementation {
    func doSomething()
}

protocol TestProtocol {
    func doSomething() -> Int
}

class TestClass: TestProtocol {

    private let sentinel: Int

    init(sentinel: Int) {
        self.sentinel = sentinel
    }

    func doSomething() -> Int {
        sentinel
    }
}

class TestAssembly: Assembly {
    func assemble(container: Container) {
        container.register(TestProtocol.self) { _  in TestClass(sentinel: 1) }
        container.register(TestProtocol.self, name: "sentinel_2") { _  in TestClass(sentinel: 2) }
    }


}

final class InjectorTests: XCTestCase {
    func testRegisteredProtocolShouldNotThrow() throws {
        Injector.shared.register([TestAssembly()])
        XCTAssertNoThrow(try Injector.shared.resolve(TestProtocol.self))
    }

    func testRequestingRegisteredProtocolShouldReturnTheRightInstance() throws {
        Injector.shared.register([TestAssembly()])
        XCTAssertNoThrow({
            let object = try Injector.shared.resolve(TestProtocol.self)
            XCTAssertTrue(object is TestClass)
        })
    }


    func testRequestingRegisteredProtocolWithUnregisteredNameShouldThrow() throws {
        Injector.shared.register([TestAssembly()])
        XCTAssertThrowsError(try Injector.shared.resolve(TestProtocol.self, name: "not existing name"))
    }

    func testRequestingRegisteredProtocolRegisteredNameShouldReturnTheRightObject() throws {
        Injector.shared.register([TestAssembly()])
        do {
            let object = try Injector.shared.resolve(TestProtocol.self, name: "sentinel_2")

            XCTAssertTrue(object.doSomething() == 2)
        } catch {
            XCTFail()
        }
    }


}
